   
    // Mẫu gọi API load all users
    function getAllUsers() {
        // create a request
        var vXmlHttObj = new XMLHttpRequest();
        vXmlHttObj.open("GET", gBASE_URL, true);
        vXmlHttObj.send();
        vXmlHttObj.onreadystatechange = function () {
            if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
                
                var vObj = JSON.parse(vXmlHttObj.responseText);
                console.log(vObj);
                //them data vao dataTable
                insertDataTable(vObj);
            }
        }
    }

    // Mẫu gọi API Get User by id
    function getUserById() {
        var vId = "1";
        var vXmlHttpObj = new XMLHttpRequest();
        vXmlHttpObj.open("GET", gBASE_URL + vId, true);
        vXmlHttpObj.send();
        vXmlHttpObj.onreadystatechange = function () {
            if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
                console.log(vXmlHttpObj.responseText);
            }
        }
    }

    // Mẫu gọi API Update user by Id
    function updateUserById() {
        // id user sẽ update
        var vUserId = "1";
        //data to be sent  (6 thuộc tính)
        var vObjectRequestData = {
            "name": "Nanette MacDearmaid",
			"email": "nmacdearmaid0@ucla.edu",
			"password": "nmacdearmaid0",
			"phone": "185-129-2326",
			"birthday": "11/18/1995",
			"gender": "Female",
        };
  
        var vXmlhttp = new XMLHttpRequest();

        vXmlhttp.open("PUT", gBASE_URL + vUserId, true);
        vXmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        vXmlhttp.send(JSON.stringify(vObjectRequestData));
        
        vXmlhttp.onreadystatechange = function() {
            if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
                console.log(vXmlhttp.responseText);
            }
        }
    }

    // Mẫu gọi API Insert user (thêm user)
    function insertUser() {
        //data to be sent (4 thuộc tính)
        var vObjectRequestData = {
            "name": "Nanette MacDearmaid",
			"email": "nmacdearmaid0@ucla.edu",
			"password": "nmacdearmaid0",
			"phone": "185-129-2326",
			"birthday": "11/18/1995",
			"gender": "Female",
        };
  
        var vXmlhttp = new XMLHttpRequest();

        vXmlhttp.open("POST", gBASE_URL, true);
        vXmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        vXmlhttp.send(JSON.stringify(vObjectRequestData));

        vXmlhttp.onreadystatechange = function() {
            if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_CREATE_SUCCESS) {
                console.log(this.responseText);
            }
        }
    }

    // Mẫu gọi API cho bài 33.40 - delete user (thêm user)
    function deleteUser() {
        // id user sẽ xóa
        var vUserId = "9";
        
        var vXmlhttp = new XMLHttpRequest();
        vXmlhttp.open("DELETE", gBASE_URL + vUserId, true);
        vXmlhttp.send();

        vXmlhttp.onreadystatechange = function() {
            if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
                console.log("Xóa thành công, user id = " + vUserId);
            }
        }
    }
