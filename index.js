
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const gREQUEST_STATUS_OK = 200;
    const gREQUEST_STATUS_CREATE_SUCCESS = 201;
    const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;
    const gBASE_URL = "https://devcamp-student-datatables.herokuapp.com/users/";
    var gUserObj = {
        "name": "",
        "email": "",
        "password": "",
        "phone": "",
        "birthday": "",
        "gender": "",
    }
    var gUserId = 0;
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();


    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        //load data list table
        loadDataListTable()
        //gui Api lay du lieu vao table
        getDataUser();

        //click button add
        $("#btn-add-user").on("click", function () {
            //show modal create
            $("#create-user-modal").modal("show");
        })

        //hàm xử lý khi xác nhận thêm dữ liệu người dùng mới
        $("#btn-create-user").on("click", function () {
            //thêm dữ liệu form vào Obj
            addDataToObj(gUserObj);
            //validate du lieu
            if (checkDataToObj(gUserObj)) {
                console.log(gUserObj);
                //gui Api them du lieu
                sendApiCreateUser();
            }
        })

          //click button update
          $("#user-table").on("click", ".edit-user", function () {
            //load du lieu vao form update
            var vUserRow = clickButtonDetail(this)
            addDataToFormUpdate(vUserRow);
            //show modal update
            $("#update-user-modal").modal("show");
          })

          //click button xac nhan update
          $("#btn-update-user").on("click", function () {
            //load du lieu tu form vao Obj
            addDataUpdateToObj(gUserObj);
            //validate du lieu
            if (checkDataToObj(gUserObj)) {
              //gui Api update user
              sendApiUpdateUSer(gUserId);
            }
          })

          //click button xoa
          $("#user-table").on("click", ".delete-user", function () {
            //hien modal xac nhan xoa
            $("#delete-confirm-modal").modal("show");
            //gan du lieu id user
            clickButtonDetail(this);
          })
          //click xác nhận xóa User
          $("#btn-confirm-delete-user").on("click", function () {
            //gui api xoa user
            sendApiDeleteUSer(gUserId);
          })
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    //ham load data list to table
    function loadDataListTable() {
        var vNameCol = [
            "id",
            "name",
            "birthday",
            "email",
            "phone",
            "action"
        ];
        /* add JSON data vào DataTable sử dụng add trực tiếp DataTable row */
        $("#user-table").DataTable({
            columns: [
                { data: vNameCol[0] },
                { data: vNameCol[1] },
                { data: vNameCol[2] },
                { data: vNameCol[3] },
                { data: vNameCol[4] },
                { data: vNameCol[5] },
            ],
            columnDefs: [
                {
                    targets: 5,
                    className: "text-center",
                    defaultContent: `
              <img class="edit-user" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
              <img class="delete-user" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
            `
                }
            ],
        })
    }
    //gui Api lay du lieu danh sach
    function getDataUser() {
        $("#user-table").find('tbody tr').remove();
        $.ajax({
            url: gBASE_URL,
            type: 'GET',
            dataType: 'json',								// kiểu dữ liệu (thường dùng khi muốn nhận dữ liệu từ server)
            async: false,									//không có thì mặc định là true 
            success: function (res) {
                console.log(res);							// dữ liệu đã được xử lý sang array
                //them du lieu vao table
                insertDataTable(res);
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText)
            }
        });
    }
    //gui Api them du lieu user
    function sendApiCreateUser() {
      $.ajax({
        url: gBASE_URL,
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(gUserObj),
        async: true,
        success: function (res) {
          console.log(res);
          //xu ly fronend
          handleInsertUserSuccess();
        },
        error: function (ajaxContext) {
          alert(ajaxContext.responseText)
        }
      });
    }
    //gui Api update user
    function sendApiUpdateUSer(vId) {
      $.ajax({
        url: gBASE_URL + vId,
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(gUserObj),
        async: true,
        success: function (res) {
          console.log(res);
          //xu ly fronend
          handleUpdateUserSuccess();
        },
        error: function (ajaxContext) {
          alert(ajaxContext.responseText)
        }
      });
    }
    //gui Api xoa user
    function sendApiDeleteUSer(vId) {
      $.ajax({
        url: gBASE_URL + vId,
        type: 'DELETE',
        contentType: 'application/json',
        async: true,
        success: function (res) {
          //xu ly fronend
          handleDeleteUserSuccess();
        },
        error: function (ajaxContext) {
          alert(ajaxContext.responseText)
        }
      });
    }
    //thêm đữ liệu vào table
    function insertDataTable(vUserObj) {
        var vTable = $("#user-table").DataTable();
        vTable.clear();
        vTable.rows.add(vUserObj);
        vTable.draw();
    }
    //ham click button detail gan du lieu Id
    function clickButtonDetail(paramElement) {
        var vRowClick = $(paramElement).closest("tr");
        var vTable = $("#user-table").DataTable();
        var vDataRow = vTable.row(vRowClick).data();
        console.log(vDataRow);
        gUserId = vDataRow.id;
        return vDataRow;
    }
    //them du lieu tu form vao Obj
    function addDataToObj(paramObj) {
        paramObj.name = $("#inp-create-name").val();
        paramObj.birthday = $("#inp-create-birthday").val();
        paramObj.email = $("#inp-create-email").val();
        paramObj.phone = $("#inp-create-phone").val();
    }
    //validate du lieu form
    function checkDataToObj(paramObj) {
        if (paramObj.name == "") {
            alert("Tên sinh viên khong duoc de trong.");
            return false;
        }

        if (paramObj.birthday == "") {
            alert("Ngày sinh khong duoc de trong.");
            return false;
        }

        if (paramObj.email == "") {
            alert("Email khong duoc de trong.");
            return false;
        }
        if (paramObj.phone == "") {
            alert("Số điện thoại khong duoc de trong.");
            return false;
        }
        return true;
    }
    //them du lieu tu obj vao form update
    function addDataToFormUpdate(vDataRow) {
        //load du lieu vao form
        $("#inp-update-name").val(vDataRow.name);
        $("#inp-update-birthday").val(vDataRow.birthday);
        $("#inp-update-email").val(vDataRow.email);
        $("#inp-update-phone").val(vDataRow.phone);
    }
    //them du lieu tu form update vao Obj
    function addDataUpdateToObj(paramObj) {
        //them du lieu vao userObj
        paramObj.name = $("#inp-update-name").val();
        paramObj.birthday = $("#inp-update-birthday").val();
        paramObj.email = $("#inp-update-email").val();
        paramObj.phone = $("#inp-update-phone").val();
    }

    // hàm xử lý hiển thị front-end khi thêm user thành công
    function handleInsertUserSuccess() {
        alert("Thêm sinh viên thành công!");
        //load lai du lieu vao table
        getDataUser();
        //xoa du lieu form
        $("#inp-create-firstname").val("");
        $("#inp-create-lastname").val("");
        $("#inp-create-subject").val("");
        $("#select-create-country").val("");
        $("#select-create-type").val("");
        $("#select-create-status").val("");
        //hide modal
        $("#create-user-modal").modal("hide");
    }
    //hàm xử lý hiển thị front-end khi update user thành công
    function handleUpdateUserSuccess() {
        alert("Sua nguoi dung thành công!");
        //load lai du lieu vao table
        getDataUser();
        //hide modal
        $("#update-user-modal").modal("hide");
    }
    //hàm xử lý hiển thị front-end khi xoa user thành công
    function handleDeleteUserSuccess() {
        alert("Xoa nguoi dung thành công!");
        //load lai du lieu vao table
        getDataUser();
        //hide modal
        $("#delete-confirm-modal").modal("hide");
    }
});
